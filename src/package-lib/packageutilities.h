// Copyright (C) 2021 The Qt Company Ltd.
// Copyright (C) 2019 Luxoft Sweden AB
// Copyright (C) 2018 Pelagicore AG
// SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only

#ifndef PACKAGEUTILITIES_H
#define PACKAGEUTILITIES_H

#include <QtAppManCommon/global.h>

QT_BEGIN_NAMESPACE_AM

namespace PackageUtilities { }

QT_END_NAMESPACE_AM

#endif // PACKAGEUTILITIES_H
